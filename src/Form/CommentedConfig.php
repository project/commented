<?php

namespace Drupal\commented\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements an example form.
 */
class CommentedConfig extends ConfigFormBase {

  /**
   * The cache menu.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $cacheMenu;
  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, MenuLinkManagerInterface $menuLinkManager, CacheBackendInterface $cacheMenu) {
    parent::__construct($config_factory);
    $this->cacheMenu = $cacheMenu;
    $this->menuLinkManager = $menuLinkManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.menu.link'),
      $container->get('cache.menu')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commented.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commented_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $activateText = ($this->config('commented.settings')->get('activated') ? 'Commented is active on your website.' : "Start using Commented on your website by simply activating it.") . ' If you don’t have an Commented account yet, please <a href="https://app.commented.io" target="_blank">click
        here</a>';

    $howtouse = '
            <h1 class="how-to-use-section__title">
                How to use Commented?
            </h1>
            <ol>
                <li>
                    Go to your website
                </li>
                <li>
                    Login to your Commented account. If you don’t have an Commented account yet, please <a
                        href="https://app.commented.io" target="_blank">click here</a>
                </li>
                <li>
                    Click on commenting activation icon.
                </li>
                <li>
                    Simply click where you want to leave a comment.
                </li>
                <li>
                    You can annotate on the image if you need
                </li>
                <li>
                    You can improve texts using AI enhancement feature.
                </li>
                <li>
                    Also, you can list the comments on your website or visit your dashboard for more details.
                </li>
            </ol>';

    $form['field_activateCommented'] = [
      '#type' => 'markup',
      '#markup' => $activateText,
      '#weight' => 1,
    ];
    $form['field_howtouse'] = [
      '#type' => 'markup',
      '#markup' => $howtouse,
      '#weight' => 1,
    ];
    $form['activated'] = [
      '#type' => 'hidden',
      // Get the inverse of the saved value.
      "#value" => $this->config('commented.settings')->get('activated') ? 0 : 1,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->config('commented.settings')->get('activated') ? $this->t('Deactivate') : $this->t('Click here to activate'),
      '#button_type' => $this->config('commented.settings')->get('activated') ? $this->t('secondary') : $this->t('primary'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commented.settings')
      ->set('activated', $form_state->getValue('activated'))
      ->save();
    $this->cacheMenu->invalidateAll();
    $this->menuLinkManager->rebuild();
  }

}
